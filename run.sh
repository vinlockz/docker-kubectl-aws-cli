#!/bin/bash

OUTPUT=$(aws ecr get-login --region us-west-2 --no-include-email)
LIST=($OUTPUT)
echo $KUBERNETES_ACCESS_TOKEN | base64 --decode > ./kube_token
echo $KUBERNETES_ACCESS_CA | base64 --decode > ./kube_ca
kubectl config set-cluster k8s --server=https://$KUBERNETES_SERVICE_HOST --certificate-authority="$(pwd)/kube_ca"
kubectl config set-credentials $KUBERNETES_USER --token="$(cat ./kube_token)"
kubectl config set-context k8s --cluster=k8s --user=$KUBERNETES_USER
kubectl config use-context k8s

eval 'kubectl delete secret aws-ecr -n='{webteamdiscordbot,iwantlasers-qa,serviceids}';' 
eval 'kubectl create secret docker-registry aws-ecr -n='{webteamdiscordbot,iwantlasers-qa,serviceids}' --docker-username=AWS --docker-password=${LIST[5]} --docker-email=$DOCKER_EMAIL --docker-server=https://$AWS_ACCOUNT_ID.dkr.ecr.us-west-2.amazonaws.com';'
